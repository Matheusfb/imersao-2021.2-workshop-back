from django.shortcuts import render, get_object_or_404, redirect
from blog.models import Post
from blog.forms import RegistroPost


def mainView(request):
    variavel = 2
    posts = Post.objects.all()
    return render(request, "main.html", {"posts":posts, "variavel":variavel})



def detailView(request, id):
    #posts = Post.objects.all()
    post_searched = get_object_or_404(Post, pk=id)
    return render(request, "detail.html", {"post_searched":post_searched, "id":id})


def createView(request):
    form = RegistroPost(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect("main")

    return render(request, "create.html", {"form":form})

def updateView(request, id):

    update_post = get_object_or_404(Post, pk=id)
    form = RegistroPost(request.POST or None, instance = update_post)

    if form.is_valid():
        form.save()
        return redirect("main")

    return render(request, "update.html", {'form':form, "update_post":update_post})


    



















""" a = input
b = input

print(a+b)

def soma(a,b):
    resultado = a+b
    return resultado

resultado = soma(1,5)
print(resultado) """
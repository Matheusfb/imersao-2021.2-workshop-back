from django.urls import path
from blog.views import mainView, detailView, createView, updateView

urlpatterns = [
    path("", mainView, name="main"),
    path("post/<int:id>/", detailView, name="detail"),
    path("adicionar-postagem/", createView, name="create"),
    path("alterar-postagem/<int:id>/", updateView, name="update")


]
